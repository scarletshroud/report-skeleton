#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/uaccess.h>
#include <linux/string.h>

#define BUF_SIZE 256
#define RESULTS_SEQUENCE_SIZE 256

MODULE_LICENSE("GPL");
MODULE_AUTHOR("scarletshroud");
MODULE_DESCRIPTION("The IO laboratory work.");

static dev_t first;

static struct cdev c_dev; 
static struct class *cl;
static struct proc_dir_entry* proc_entry;

static uint32_t sequence_indx = 0;
static uint32_t results_sequence[RESULTS_SEQUENCE_SIZE];

#define STATE_BUSY 1
#define STATE_FREE 0

static uint8_t state = STATE_FREE; 

static int dev_open(struct inode *i, struct file *f) {
	if (state == STATE_BUSY) {
		printk(KERN_INFO "Driver is busy.");
		return -1; 
	}
	
	state = STATE_BUSY;
  printk(KERN_INFO "Driver is opened.");
  
  return 0;
}

static int dev_close(struct inode *i, struct file *f) {
  printk(KERN_INFO "Driver is closed.");
  state = STATE_FREE;
  return 0;
}

static void dmesg_print_sequence(void) {
	size_t i = 0;
	printk("Results:"); 
	for (; i < sequence_indx; i++) {
		printk("%d", results_sequence[i]);
	}
}

static ssize_t dev_read(struct file *f, char __user *ubuf, size_t len, loff_t *ppos) {	
	int msg_len;
	msg_len = sequence_indx;
	
	printk(KERN_INFO "Driver: read");

	if (*ppos != 0) {
		printk(KERN_INFO "Unable to read device."); 
		return 0;
	}

	dmesg_print_sequence(); 

	*ppos += msg_len;

	return 0;
}

#define MSG_MAX_SIZE 1024
static const char* const default_msg = "Sequence is empty.\n";
static ssize_t proc_read_test(struct file *f, char __user *ubuf, size_t len, loff_t *ppos) {
	size_t msg_len, local_msg_len, i; 
	char msg[MSG_MAX_SIZE] = ""; 
	char local_msg[32];
	
	printk(KERN_INFO "Proc: read");
	
	if (*ppos != 0) {
		return 0;
	}	
		
	if (sequence_indx == 0) {
		msg_len = strlen(default_msg);
		
		if (copy_to_user(ubuf, default_msg, msg_len) < 0)
			return -EFAULT;
				
		*ppos += msg_len;
		return msg_len;
	}
	
	i = 0;
	msg_len = 0;
	 
	for (; i < sequence_indx; i++) {
		
		local_msg_len = sprintf(local_msg, "The length of text %ld = %d\n", i + 1, results_sequence[i]);
				
		if (local_msg_len < 0)
			return -EFAULT;
					
		msg_len += local_msg_len;
		
		if (msg_len > MSG_MAX_SIZE) {
			return -EFAULT;
		}
		
		strcat(msg, local_msg);
	} 
	
	if (copy_to_user(ubuf, msg, msg_len) < 0) 
		return -EFAULT;
	
	*ppos += msg_len;
	
	return msg_len;
}

static ssize_t dev_write(struct file *f, const char __user *ubuf,  size_t len, loff_t *ppos) {
  	printk(KERN_INFO "Driver: write");
   
	if (*ppos > 0) {
		printk(KERN_INFO "Passed message to char dev is too big ))."); 
		return 0;
	}

	results_sequence[sequence_indx++] = len - 1;	
	
	*ppos += len;

	return len;
}

static struct file_operations mychdev_fops = {
	.owner = THIS_MODULE,
	.open = dev_open,
	.release = dev_close,
	.read = dev_read,
	.write = dev_write
};

static const struct proc_ops proc_fops = { 
	.proc_open = dev_open,
	.proc_read = proc_read_test,
	.proc_release = dev_close
};


#define PROC_NAME "var1"
static int proc_init(void) {
	if ((proc_entry = proc_create(PROC_NAME, 0666, NULL, &proc_fops)) == NULL) {
			return -1; 
	} 
	
	printk(KERN_INFO "%s: proc file is created\n", THIS_MODULE->name);
	return 0;
}

static void proc_destroy(void) {
	proc_remove(proc_entry);
	printk(KERN_INFO "%s: proc file is deleted\n", THIS_MODULE->name);
}

#define DRIVER_NAME "var1_driver"
#define DRIVER_CLASS "var1_driver_class"
#define DEVICE_NAME "var1"
static int __init ch_drv_init(void) {
    printk(KERN_INFO "Char driver init...\n");
    
    if (alloc_chrdev_region(&first, 0, 1, DRIVER_NAME) < 0) {
		return -1;
    }
	  
    if ((cl = class_create(THIS_MODULE, DRIVER_CLASS)) == NULL) {
		unregister_chrdev_region(first, 1);
		return -1;
  	}
	  
    if (device_create(cl, NULL, first, NULL, DEVICE_NAME) == NULL) {
			class_destroy(cl);
			unregister_chrdev_region(first, 1);
			return -1;
	  }
	  
    cdev_init(&c_dev, &mychdev_fops);
    
    if (cdev_add(&c_dev, first, 1) == -1) {
			device_destroy(cl, first);
			class_destroy(cl);
			unregister_chrdev_region(first, 1);
			return -1;
	  }
	  
	if (proc_init() == -1) {
		cdev_del(&c_dev);
		device_destroy(cl, first);
		class_destroy(cl);
		unregister_chrdev_region(first, 1);
		return -1;
	}
	  
    return 0;
}
 
static void __exit ch_drv_exit(void) {
    cdev_del(&c_dev);
    device_destroy(cl, first);
    class_destroy(cl);
    unregister_chrdev_region(first, 1);
    proc_destroy();
    sequence_indx = 0;
    printk(KERN_INFO "Char driver was successfully destroyed.");
}

module_init(ch_drv_init);
module_exit(ch_drv_exit);
