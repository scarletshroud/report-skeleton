# Лабораторная работа 1

**Название:** "Разработка драйверов символьных устройств"

**Цель работы:** "получить знания и навыки разработки драйверов символьных 
устройств для операционной системы Linux."

## Описание функциональности драйвера

Драйвер создает два файла в системе:
    - /dev/var1
    - /proc/var1

1. При записи в файл символьного устройства выполняется посчет переданных символов и сохранения результата. 
2. При чтении из файла символьного устройства выполняется вывод результатов в кольцевой буфер. 
3. При чтении из файла /proc/var1 выполняется вывод результатов в терминал пользователя.


## Инструкция по сборке

**Использовать утилиту make для сборки проекта.**
- make - собрать проект
- make clean - очистить проект


## Инструкция пользователя

1. Собрать модуль ядра
2. Загрузить модуль ядра
3. Создать узел для устройства
4. Писать в узел текст
5. Читать из узла для получение ранее вычисленных выражений, на одно чтение по кругу выводятся последовательно пять последних результатов
6. При чтении из файла /proc/var1 проверять лог для того, чтобы увидеть результат чтения


## Примеры использования
![Иллюстрация к проекту 1](lab1/img/2.jpg)
![Иллюстрация к проекту 2](lab1/img/1.jpg)
